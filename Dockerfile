FROM ubuntu:18.04 as ansible
COPY . /ansible
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-add-repository --yes --update ppa:ansible/ansible && \
    apt-get install -y ansible && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/* /var/cache/apt
ENTRYPOINT [ "ansible-playbook", "playbook.yml", "-i", "hosts" ]
WORKDIR /ansible
ENV HOME=/ansible
EXPOSE 15672 5672