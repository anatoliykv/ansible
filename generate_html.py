from yattag import Doc
import sys

doc, tag, text = Doc().tagtext()

doc.asis('<!DOCTYPE html>')
with tag('html'):
    with tag('body'):
        with tag('h1', id = 'main', align="center"):
            text('RabbitMQ by Ansible')

        with tag('h2', id = 'main2', align="center"):
            text('Pipeline status')
        with tag('a', href='https://gitlab.com/anatoliykv/ansible/-/commits/master'):
            doc.stag('img', src='https://gitlab.com/anatoliykv/ansible/badges/master/pipeline.svg', style='margin-top: 6em', width='100%')


result = doc.getvalue()

sys.stdout = open('index.html', 'w')
print (result)